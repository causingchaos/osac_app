class UsersController < ApplicationController

  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user,     only: :destroy

  def index
    @users = User.paginate(page: params[:page])
    #@users = User.all
  end

  #this is public for all users
  def show
    #really a string "id" but rails converts to integer
    @user = User.find(params[:id])
    #Note: this @user could be removed bc/ we assign the variable when we call
    #correct_user, leaving for now (extra database call frowned reduction)

    #using the byebug which shows (byebug in server window)
    #debugger
  end

  def new
    @user = User.new
  end

  def create
    #note we can access :user because of the hash that is made
    #when converting rails code into html form information
    @user = User.new(user_params)
    if @user.save
      log_in @user   #log in via method in sessions controller helper.rb
      #if successfull, display on next page
      flash[:success] = "Welcome to OVSAC!"
      redirect_to(user_url(@user))
    else
      render('new')
    end
  end

  def edit
    #Note: this @user could be removed bc/ we assign the variable when we call
    #correct_user, leaving for now (extra database call frowned reduction)
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      #Handle a successful update
      flash[:success] = "Profile updated"
      redirect_to(@user)
    else
      render 'edit.html.erb'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  #indentation is simply for shoing these methods are inside private.
  private
    #use strong parameters to prevent mass assignment vulnerablity
    def user_params
      params.require(:user).permit(:name,:email,:password,:password_confirmation,:ucid)
    end

    #Before filters
    def logged_in_user
      unless logged_in?    #note: method in session_controlelr helper
        store_location   #This stores the current page location on site, if login fails
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    #Filter: Confirms a correct user.
    def correct_user
      @user = User.find(params[:id])    #Note current_user defined in session_helper
      #redirect_to(root_url) unless @user == current_user
      redirect_to(root_url) unless current_user?(@user)
    end

    # Confirms an admin user.
    def admin_user
    redirect_to(root_url) unless current_user.admin?
    end

end













