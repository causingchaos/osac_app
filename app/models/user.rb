class User < ApplicationRecord

  #before_save {email.downcase!} this will apply immediate effects to email using bang.
  before_save {self.email = email.downcase}
  validates(:name, presence: true, length: {maximum: 50})
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates(:email, presence: true, length: {maximum: 255}, format: {with: VALID_EMAIL_REGEX}, uniqueness:
  {case_sensitive: false})
  #rails : on uniqueness, rails just infers that uniqueness = true

  has_secure_password
  #this has a allow_nil for blank passwords, strictj
  validates :password, presence: true, length: {minimum: 6}, allow_nil: true

  #Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)

  end

end
