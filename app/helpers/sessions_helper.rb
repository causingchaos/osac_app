module SessionsHelper

  #Logs in the given user.
  def log_in(user)
    session[:user_id] = user.id
  end

  def current_user?(user)
    user == current_user
  end

  #Returns the current logged-in user (if any)
  def current_user
    #if @current_user doesn't exist, run the alternate statement and fetch it.
    @current_user = @current_user ||= User.find_by(id: session[:user_id])
  end

  #Returns true if the user is logged in, false otherwise
  def logged_in?
    !current_user.nil?
  end

  def log_out
    session.delete(:user_id)
    @current_user = nil
  end

  #Redirects to stored location (or to the default).
  #Notes: Same as session facility we used for logging users in
  # uses the request object (via request.original_url) to get the URL of the requested page
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  #Stores the URL trying to be accessed.
  #Notes: the store_location method puts the requested URL in the sessions variable under the key
  #:forwarding URL if a user, say, submits a form when not logged in

  #if a user is not logged in, and clicks submit a form. (i.e. the user deleted session cookies by hand)
  #before submitting. the resulting redirect would issue a GET request to the URL expecitng POST, PATCH
  #DELETE, thereby causing an error. Including if request.get? prevents this from happening.
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end

end
