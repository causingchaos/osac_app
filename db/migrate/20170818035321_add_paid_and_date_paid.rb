class AddPaidAndDatePaid < ActiveRecord::Migration[5.1]
    def change
      add_column :users, :is_paid, :boolean, default: false
      add_column :users, :date_paid, :date
      add_column :users, :major, :string
    end
  end
