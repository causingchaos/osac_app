class AddUcidToUsers < ActiveRecord::Migration[5.1]

  def change
    add_column("users","ucid",:string, :limit => 12)
  end

end
