Rails.application.routes.draw do

  root 'static_pages#home'

  #create custom paths. i.e. user enters /signup, it redirects to users#new
  get '/home', to: 'static_pages#home'
  get '/help', to: 'static_pages#help'
  get '/ourStory', to: 'static_pages#about'
  get '/clubEvents', to: 'static_pages#events'
  get '/whoarewe', to: 'static_pages#contact'
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  #adding app resources
  #Users table(id,name,email,password_digest)
  #Converts to a RESTful resource
  resources :users
end
